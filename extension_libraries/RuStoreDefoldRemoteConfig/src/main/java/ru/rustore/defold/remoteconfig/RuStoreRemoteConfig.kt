package ru.rustore.defold.remoteconfig

import android.net.Uri
import android.util.Log
import com.google.gson.GsonBuilder
import ru.rustore.defold.core.JsonBuilder
import ru.rustore.defold.core.RuStoreCore
import ru.rustore.defold.core.UriTypeAdapter
import ru.rustore.defold.remoteconfig.model.DefoldRemoteConfigClientEventListener
import ru.rustore.sdk.remoteconfig.RemoteConfigClient

object RuStoreRemoteConfig: DefoldRemoteConfigClientEventListener {
	private const val CHANNEL_GET_REMOTE_CONFIG_SUCCESS = "rustore_get_remote_config_success"
	private const val CHANNEL_GET_REMOTE_CONFIG_FAILURE = "rustore_get_remote_config_failure"
	private const val CHANNEL_BACKGROUND_JOB_ERRORS = "rustore_background_job_errors"
	private const val CHANNEL_FIRST_LOAD_COMPLETE = "rustore_first_load_complete"
	private const val CHANNEL_INIT_COMPLETE = "rustore_init_complete"
	private const val CHANNEL_MEMORY_CACHE_UPDATED = "rustore_memory_cache_updated"
	private const val CHANNEL_PERSISTENT_STORAGE_UPDATED = "rustore_persistent_storage_updated"
	private const val CHANNEL_REMOTE_CONFIG_NETWORK_REQUEST_FAILURE = "rustore_remote_config_network_request_failure"

	private val gson = GsonBuilder()
		.registerTypeAdapter(Uri::class.java, UriTypeAdapter())
		.create()

	fun initEventListener() {
		RemoteConfigClientEventListenerDefault.setListener(this)
	}

	fun getRemoteConfig() {
		RemoteConfigClient.instance.getRemoteConfig()
			.addOnSuccessListener { result ->
				RuStoreCore.emitSignal(CHANNEL_GET_REMOTE_CONFIG_SUCCESS, gson.toJson(result))
			}
			.addOnFailureListener { throwable ->
				RuStoreCore.emitSignal(CHANNEL_GET_REMOTE_CONFIG_FAILURE, JsonBuilder.toJson(throwable))
			}
	}

	fun setAccount(value: String) {
		ConfigRequestParameterProviderDefault.setAccount(value)
	}

	fun getAccount(): String = ConfigRequestParameterProviderDefault.getAccount()

	fun setLanguage(value: String) {
		ConfigRequestParameterProviderDefault.setLanguage(value)
	}

	fun getLanguage(): String = ConfigRequestParameterProviderDefault.getLanguage()

	override fun backgroundJobErrors(exception: Throwable) =
		RuStoreCore.emitSignal(CHANNEL_BACKGROUND_JOB_ERRORS, JsonBuilder.toJson(exception))
	override fun firstLoadComplete() = RuStoreCore.emitSignal(CHANNEL_FIRST_LOAD_COMPLETE, String())
	override fun initComplete() = RuStoreCore.emitSignal(CHANNEL_INIT_COMPLETE, String())
	override fun memoryCacheUpdated() = RuStoreCore.emitSignal(CHANNEL_MEMORY_CACHE_UPDATED, String())
	override fun persistentStorageUpdated() = RuStoreCore.emitSignal(CHANNEL_PERSISTENT_STORAGE_UPDATED, String())
	override fun remoteConfigNetworkRequestFailure(throwable: Throwable) =
		RuStoreCore.emitSignal(CHANNEL_REMOTE_CONFIG_NETWORK_REQUEST_FAILURE, JsonBuilder.toJson(throwable))
}
