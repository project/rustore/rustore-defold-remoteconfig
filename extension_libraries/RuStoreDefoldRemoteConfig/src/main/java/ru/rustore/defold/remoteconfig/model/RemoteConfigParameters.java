package ru.rustore.defold.remoteconfig.model;

public interface RemoteConfigParameters {

    String getAppBuild();
    String getAppVersion();
    String getDeviceId();
    String getDeviceModel();
    String getEnvironment();
    String getOsVersion();
}
