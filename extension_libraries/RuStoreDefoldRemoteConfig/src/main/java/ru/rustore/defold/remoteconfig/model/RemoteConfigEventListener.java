package ru.rustore.defold.remoteconfig.model;

public interface RemoteConfigEventListener {
    void backgroundJobErrors(Throwable exception);
    void firstLoadComplete();
    void initComplete();
    void memoryCacheUpdated();
    void persistentStorageUpdated();
    void remoteConfigNetworkRequestFailure(Throwable throwable);
}
