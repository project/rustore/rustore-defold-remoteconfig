package ru.rustore.defold.remoteconfig.model;

public interface DefoldRemoteConfigClientEventListener {
    void backgroundJobErrors(Throwable exception);
    void firstLoadComplete();
    void initComplete();
    void memoryCacheUpdated();
    void persistentStorageUpdated();
    void remoteConfigNetworkRequestFailure(Throwable throwable);
}
