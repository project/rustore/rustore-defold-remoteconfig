package ru.rustore.defold.remoteconfig.model

enum class DefoldUpdateBehaviour {
    Actual,
    Default,
    Snapshot
}
