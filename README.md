## RuStore Defold плагин для работы с облачным сервисом конфигурации приложения

### [🔗 Документация разработчика][10]

SDK Remote Config это облачный сервис, который позволяет изменять поведение и внешний вид вашего приложения, не требуя от пользователей загрузки обновления приложения. SDK инкапсулирует в себе запрос конфигурации с сервера, кэширование, фоновое обновление.

Репозиторий содержит плагины “RuStoreRemoteConfig”, “RuStoreCore”, “RuStoreApplication”, а также демонстрационное приложение с примерами использования и настроек. Поддерживаются версии Defold 1.6.2+.


### Сборка примера приложения

Вы можете ознакомиться с демонстрационным приложением содержащим представление работы всех методов sdk:
- [README](remoteconfig_example/README.md)
- [remoteconfig_example](https://gitflic.ru/project/rustore/rustore-defold-remoteconfig/file?file=remoteconfig_example)


### Установка плагина в свой проект

1. Скопируйте папки _“remoteconfig_example / extension_rustore_remoteconfig”_, _“remoteconfig_example / extension_rustore_core”_, _“remoteconfig_example / extension_rustore_application”_ в корень вашего проекта.

2. В файле манифеста вашего проекта замените класс _“android.support.multidex.MultiDexApplication”_ на _“ru.rustore.defold.remoteconfig.DefoldRemoteConfigApplication”_.


### Пересборка плагина

Если вам необходимо изменить код библиотек плагинов, вы можете внести изменения и пересобрать подключаемые .jar файлы.

1. Откройте в вашей IDE проект Android из папки _“extension_libraries”_.

2. Выполните сборку проекта командой gradle assemble.

При успешном выполнении сборки в папках _“remoteconfig_example / extension_rustore_remoteconfig / lib / android”_ и _“remoteconfig_example / extension_rustore_core / lib / android”_ будут обновлены файлы:
- RuStoreDefoldRemoteConfig.jar
- RuStoreDefoldCore.jar


### История изменений

[CHANGELOG](CHANGELOG.md)


### Условия распространения

Данное программное обеспечение, включая исходные коды, бинарные библиотеки и другие файлы распространяется под лицензией MIT. Информация о лицензировании доступна в документе [MIT-LICENSE](MIT-LICENSE.txt).


### Техническая поддержка

Дополнительная помощь и инструкции доступны на странице [help.rustore.ru](https://help.rustore.ru/) и по электронной почте [support@rustore.ru](mailto:support@rustore.ru).

[10]: https://www.rustore.ru/help/developers/tools/remote-config/sdk/defold/1-1-1
