SECOND_RECEIVER = "second#second"
SECOND_SET_REQUEST_PARAMETERS = "set_request_parameters"
SECOND_GET_REMOTE_CONFIG = "get_remote_config"
SECOND_CLEAR_LOG = "clear_log"
SECOND_MOVE_LEFT = 'move_left'
SECOND_MOVE_RIGHT = 'move_right'

SECOND_SET_RECEIVER_MESSAGE_ID = "second_set_receiver_message_id"
SECOND_SET_ENABLED_MESSAGE_ID = "second_set_enabled_message_id"
SECOND_SET_ACCOUNT_MESSAGE_ID = 'second_set_account_message_id'
SECOND_SET_LANGUAGE_MESSAGE_ID = 'second_set_language_message_id'
SECOND_LOG_ADD_TEXT_MESSAGE_ID = "second_log_add_text_message_id"
SECOND_LOG_CLEAR_MESSAGE_ID = "second_log_clear_message_id"
SECOND_BUTTON_PRESSED = "second_button_pressed"
