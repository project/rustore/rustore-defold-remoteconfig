#define EXTENSION_NAME RuStoreRemoteConfig
#define LIB_NAME "RuStoreRemoteConfig"
#define MODULE_NAME "rustoreremoteconfig"

#if defined(DM_PLATFORM_ANDROID)

#include <dmsdk/sdk.h>
#include <dmsdk/dlib/android.h>
#include "AndroidJavaObject.h"

using namespace RuStoreSDK;

static void GatJavaObjectInstance(JNIEnv* env, AndroidJavaObject* instance)
{
    jclass cls = dmAndroid::LoadClass(env, "ru.rustore.defold.remoteconfig.RuStoreRemoteConfig");
    jfieldID instanceField = env->GetStaticFieldID(cls, "INSTANCE", "Lru/rustore/defold/remoteconfig/RuStoreRemoteConfig;");
    jobject obj = env->GetStaticObjectField(cls, instanceField);

    instance->cls = cls;
    instance->obj = obj;
}

static int InitEventListener(lua_State* L)
{
    DM_LUA_STACK_CHECK(L, 0);

    dmAndroid::ThreadAttacher thread;
    JNIEnv* env = thread.GetEnv();

    AndroidJavaObject instance;
    GatJavaObjectInstance(env, &instance);
    jmethodID method = env->GetMethodID(instance.cls, "initEventListener", "()V");
    env->CallVoidMethod(instance.obj, method);

    thread.Detach();

    return 0;
}

static int GetRemoteConfig(lua_State* L)
{
    DM_LUA_STACK_CHECK(L, 0);

    dmAndroid::ThreadAttacher thread;
    JNIEnv* env = thread.GetEnv();

    AndroidJavaObject instance;
    GatJavaObjectInstance(env, &instance);
    jmethodID method = env->GetMethodID(instance.cls, "getRemoteConfig", "()V");
    env->CallVoidMethod(instance.obj, method);

    thread.Detach();

    return 0;
}

static int SetAccount(lua_State* L)
{
    DM_LUA_STACK_CHECK(L, 0);
    
    dmAndroid::ThreadAttacher thread;
    JNIEnv* env = thread.GetEnv();

    const char* text = (char*)luaL_checkstring(L, 1);
    jstring jtext = env->NewStringUTF(text);

    AndroidJavaObject instance;
    GatJavaObjectInstance(env, &instance);
    jmethodID method = env->GetMethodID(instance.cls, "setAccount", "(Ljava/lang/String;)V");
    env->CallVoidMethod(instance.obj, method, jtext);

    env->DeleteLocalRef(jtext);

    thread.Detach();
    
    return 0;
}

static int GetAccount(lua_State* L)
{
    DM_LUA_STACK_CHECK(L, 1);

    dmAndroid::ThreadAttacher thread;
    JNIEnv* env = thread.GetEnv();

    AndroidJavaObject instance;
    GatJavaObjectInstance(env, &instance);
    jmethodID method = env->GetMethodID(instance.cls, "getAccount", "()Ljava/lang/String;");
    jstring jtext = (jstring)env->CallObjectMethod(instance.obj, method);

    const char* ctext = env->GetStringUTFChars(jtext, nullptr);
    lua_pushstring(L, ctext);
    env->ReleaseStringUTFChars(jtext, ctext);

    thread.Detach();

    return 1;
}

static int SetLanguage(lua_State* L)
{
    DM_LUA_STACK_CHECK(L, 0);

    dmAndroid::ThreadAttacher thread;
    JNIEnv* env = thread.GetEnv();

    const char* text = (char*)luaL_checkstring(L, 1);
    jstring jtext = env->NewStringUTF(text);

    AndroidJavaObject instance;
    GatJavaObjectInstance(env, &instance);
    jmethodID method = env->GetMethodID(instance.cls, "setLanguage", "(Ljava/lang/String;)V");
    env->CallVoidMethod(instance.obj, method, jtext);

    env->DeleteLocalRef(jtext);

    thread.Detach();

    return 0;
}

static int GetLanguage(lua_State* L)
{
    DM_LUA_STACK_CHECK(L, 1);

    dmAndroid::ThreadAttacher thread;
    JNIEnv* env = thread.GetEnv();

    AndroidJavaObject instance;
    GatJavaObjectInstance(env, &instance);
    jmethodID method = env->GetMethodID(instance.cls, "getLanguage", "()Ljava/lang/String;");
    jstring jtext = (jstring)env->CallObjectMethod(instance.obj, method);

    const char* ctext = env->GetStringUTFChars(jtext, nullptr);
    lua_pushstring(L, ctext);
    env->ReleaseStringUTFChars(jtext, ctext);

    thread.Detach();

    return 1;
}

// Functions exposed to Lua
static const luaL_reg Module_methods[] =
{
    {"init_event_listener", InitEventListener},
    {"get_remote_config", GetRemoteConfig},
    {"set_account", SetAccount},
    {"get_account", GetAccount},
    {"set_language", SetLanguage},
    {"get_language", GetLanguage},
    {0, 0}
};

#endif

static void LuaInit(lua_State* L)
{
    int top = lua_gettop(L);
    
    luaL_register(L, MODULE_NAME, Module_methods);

    lua_pop(L, 1);
    assert(top == lua_gettop(L));
}

static dmExtension::Result AppInitializeRuStoreRemoteConfigExtension(dmExtension::AppParams* params)
{
    return dmExtension::RESULT_OK;
}

static dmExtension::Result InitializeRuStoreRemoteConfigExtension(dmExtension::Params* params)
{
    LuaInit(params->m_L);
    return dmExtension::RESULT_OK;
}

static dmExtension::Result AppFinalizeRuStoreRemoteConfigExtension(dmExtension::AppParams* params)
{
    return dmExtension::RESULT_OK;
}

static dmExtension::Result FinalizeRuStoreRemoteConfigExtension(dmExtension::Params* params)
{
    return dmExtension::RESULT_OK;
}

DM_DECLARE_EXTENSION(EXTENSION_NAME, LIB_NAME, AppInitializeRuStoreRemoteConfigExtension, AppFinalizeRuStoreRemoteConfigExtension, InitializeRuStoreRemoteConfigExtension, nullptr, nullptr, FinalizeRuStoreRemoteConfigExtension)
