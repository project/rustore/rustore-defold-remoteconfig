package ru.rustore.defold.remoteconfig;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import ru.rustore.sdk.remoteconfig.UpdateBehaviour;
import ru.rustore.defold.remoteconfig.model.DefoldUpdateBehaviour;
import ru.rustore.defold.remoteconfig.RuStoreDefoldRemoteConfigBuilder;

public class DefoldRemoteConfigApplication extends Application {
    public final String APP_ID = "1dca9044-c204-47ea-9618-fafe3af30f1a";
    //public final String APP_ID = "a83c91d3-21b4-4891-841e-0ed0fc39a562";
    public final DefoldUpdateBehaviour UPDATE_BEHAVIOUR = DefoldUpdateBehaviour.Actual;
    public final int UPDATE_TIME = 15;
    public final String ACCOUNT = "Auff";
    public final String LANGUAGE = "ru";

    @Override
    public void onCreate() {
        super.onCreate();

        SharedPreferences preferences = getSharedPreferences("rustore_shared_preferences", Context.MODE_PRIVATE);

        int updateTime = preferences.getInt("rustore_update_time", UPDATE_TIME);
        String updateBehaviourName = preferences.getString("rustore_update_behaviour", "Actual");
        DefoldUpdateBehaviour updateBehaviour = DefoldUpdateBehaviour.valueOf(updateBehaviourName);
        String account = preferences.getString("rustore_account", ACCOUNT);
        String language = preferences.getString("rustore_language", LANGUAGE);

        RuStoreDefoldRemoteConfigBuilder.INSTANCE.setAccount(account);
        RuStoreDefoldRemoteConfigBuilder.INSTANCE.setLanguage(language);
        RuStoreDefoldRemoteConfigBuilder.INSTANCE.init(APP_ID, updateBehaviour, updateTime, null, null, getApplicationContext());
    }
}
