#define EXTENSION_NAME RuStoreApplication
#define LIB_NAME "RuStoreApplication"
#define MODULE_NAME "rustoreapplication"

#if defined(DM_PLATFORM_ANDROID)

#include <dmsdk/sdk.h>
#include <dmsdk/dlib/android.h>

#endif

DM_DECLARE_EXTENSION(EXTENSION_NAME, LIB_NAME, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr)
