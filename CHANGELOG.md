## История изменений

### Release 1.1.1
- Версия SDK remoteconfig 1.1.1.


### Release 1.1.0
- Версия SDK remoteconfig 1.1.0.
- Добавлено поле `simpleName` в JSON ошибок.


### Release 1.0.0
- Версия SDK remoteconfig 1.0.0.
